This crate aims to provide various tools for slices.

This crate use its own streaming iterator for some slice extensions
due to the lack of generic associated type in the language:
you therefore cannot use those iterators with the `for` control flow.
Use the `while let` control flow, or the macro helper, as you can see below.

Example
=======

**Cargo.toml**:

```
[dependencies]
slicetools = "0.4.*"
```

**main.rs**:

```rust
extern crate slicetools;

use slicetools::*;

let mut v = vec![1, 2, 3, 4];
{
    let mut it = v.pairs_mut();
    
    while let Some((a, b)) = it.next() {
        if *b > *a {
            *a += 1;
        }
    }
}
assert_eq!(v, &[4, 4, 4, 4]);
```

Or, with the helper macro:

```rust
#[macro_use] extern crate slicetools;

use slicetools::*;

let mut v = vec![1, 2, 3, 4];

for_each!( (a, b) in v.pairs_mut() => {
    if *b > *a {
        *a += 1;
    }
});
assert_eq!(v, &[4, 4, 4, 4]);
```

Changelog
=========

### 0.4.*

* Full refactoring, rewriting the custom iterator with associated type.

### 0.3.*

* Add 3 new extensions to take groups of items.

### 0.2.*

* Use own streaming iterator instead of regular iterator that was unsafe (allowed multiple mutable borrowing on the same item).

* Add a new streamer for doing a cartesian product between two slices.
