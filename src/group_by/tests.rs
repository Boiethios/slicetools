use super::GroupBy;

#[test]
fn slice_with_0_item() {
    let v: Vec<i32> = vec![];
    let mut it = GroupBy::new(&v);
    
    assert!(it.next().is_none());
}

#[test]
fn slice_with_1_item() {
    let v = vec![0];
    let mut it = GroupBy::new(&v);

    assert_eq!(it.next(), Some(&[0][..]));
    assert!(it.next().is_none());
}

#[test]
fn slice_with_unique_items() {
    let v = vec![0, 1, 2];
    let mut it = GroupBy::new(&v);

    assert_eq!(it.next(), Some(&[0][..]));
    assert_eq!(it.next(), Some(&[1][..]));
    assert_eq!(it.next(), Some(&[2][..]));
    assert!(it.next().is_none());
}

#[test]
fn slice_with_one_item_type() {
    let v = vec![0, 0, 0, 0];
    let mut it = GroupBy::new(&v);

    assert_eq!(it.next(), Some(&v[..]));
    assert!(it.next().is_none());
}

#[test]
fn slice_with_various_groups() {
    let v = vec![0, 0, 1, 2, 2, 2];
    let mut it = GroupBy::new(&v);

    assert_eq!(it.next(), Some(&[0, 0][..]));
    assert_eq!(it.next(), Some(&[1][..]));
    assert_eq!(it.next(), Some(&[2, 2, 2][..]));
    assert!(it.next().is_none());
}
