#[cfg(test)] mod tests;

pub struct GroupBy<'a, T: 'a, C, K> where C: Fn<(&T) -> K>, K: PartialEq {
    slice: &'a [T],
}

impl<'a, T> Iterator for GroupBy<'a, T> where T: PartialEq {
    type Item = &'a [T];
    
    fn next(&mut self) -> Option<Self::Item> {
        let first = match self.slice.first() {
            None => return None,
            Some(first) => first,
        };
        let end = self.slice.iter().take_while(|i| *i == first).count();
        let (result, slice) = self.slice.split_at(end);
        self.slice = slice;
        Some(result)
    }
}

impl<'a, T: 'a, C, K> GroupBy<'a, T: 'a, C, K> where C: Fn<(&T) -> K>, K: PartialEq {
    pub fn new(slice: &'a [T]) -> GroupBy<'a, T> {
        GroupBy{ slice }
    }
}