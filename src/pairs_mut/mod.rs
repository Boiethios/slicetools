#[cfg(test)]
mod tests;

use BoundedIterator;

pub struct PairsMut<'a, T: 'a> {
    slice: &'a mut [T],
    first: usize,
    second: usize,
}

impl<'a, 'item, T: 'item> BoundedIterator<'item> for PairsMut<'a, T> {
    type Item = (&'item mut T, &'item mut T);

    fn next_item(&'item mut self) -> Option<Self::Item> {
        match self.slice.len() - self.first {
            0 | 1 => None,
            _ => self.get_next(),
        }
    }
}

impl<'a, T> PairsMut<'a, T> {
    pub fn new(slice: &'a mut [T]) -> PairsMut<'a, T> {
        PairsMut {
            slice,
            first: 0,
            second: 1,
        }
    }

    fn get_next(&mut self) -> Option<(&mut T, &mut T)> {
        let ptr = self.slice.as_mut_ptr();

        if self.second >= self.slice.len() {
            self.first += 1;
            self.second = self.first + 1;
        }

        if self.second < self.slice.len() {
            //    second < len
            debug_assert!(self.first < self.second); // && first < second
                                                     // => this is safe
            let res = unsafe { Self::get_pair(ptr, self.first, self.second) };
            self.second += 1;
            res
        } else {
            None
        }
    }

    unsafe fn get_pair<'item>(
        ptr: *mut T,
        first: usize,
        second: usize,
    ) -> Option<(&'item mut T, &'item mut T)> {
        Some((
            &mut *ptr.offset(first as isize),
            &mut *ptr.offset(second as isize),
        ))
    }
}
