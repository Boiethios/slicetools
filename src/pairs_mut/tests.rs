use super::PairsMut;
use BoundedIterator;

#[test]
fn empty_slice() {
    let input: &mut [i32] = &mut [];
    let mut it = PairsMut::new(input);

    assert!(it.next_item().is_none());
}

#[test]
fn slice_with_1_item() {
    let input = &mut [0];
    let mut it = PairsMut::new(input);

    assert_eq!(it.next_item(), None);
}

#[test]
fn slice_with_2_items() {
    let input = &mut [0, 1];
    let mut it = PairsMut::new(input);

    assert_eq!(it.next_item(), Some((&mut 0, &mut 1)));
    assert_eq!(it.next_item(), None);
}

#[test]
fn slice_with_3_items() {
    let input = &mut [0, 1, 2];
    let mut it = PairsMut::new(input);

    assert_eq!(it.next_item(), Some((&mut 0, &mut 1)));
    assert_eq!(it.next_item(), Some((&mut 0, &mut 2)));
    assert_eq!(it.next_item(), Some((&mut 1, &mut 2)));
    assert_eq!(it.next_item(), None);
}

#[test]
fn simple_algorith() {
    let mut v = vec![1, 2, 3, 4];
    {
        let mut it = PairsMut::new(&mut v);

        while let Some((a, b)) = it.next_item() {
            if *b > *a {
                *a += 1;
            }
        }
    }
    assert_eq!(v, &[4, 4, 4, 4]);
}
