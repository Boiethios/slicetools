pub trait BoundedIterator<'item> {
    type Item;

    fn next_item(&'item mut self) -> Option<Self::Item>;

    fn enumerate(self) -> Enumerate<Self>
    where
        Self: Sized,
    {
        Enumerate {
            iter: self,
            count: 0,
        }
    }
}

// Enumerate

pub struct Enumerate<I> {
    iter: I,
    count: usize,
}

impl<'item, I> BoundedIterator<'item> for Enumerate<I>
where
    I: BoundedIterator<'item>,
{
    type Item = (usize, <I as BoundedIterator<'item>>::Item);

    fn next_item(&'item mut self) -> Option<Self::Item> {
        match self.iter.next_item() {
            None => None,
            Some(item) => {
                let ret = (self.count, item);
                self.count += 1;
                Some(ret)
            }
        }
    }
}
