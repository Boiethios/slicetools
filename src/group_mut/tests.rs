#[test]
fn slice_with_0_item() {
    let input: &mut [i32] = &mut [];
    let mut it = super::new(input);

    assert!(it.next().is_none());
}

#[test]
fn slice_with_1_item() {
    let input = &mut [0];
    let mut it = super::new(input);

    assert_eq!(it.next(), Some(&mut [0][..]));
    assert_eq!(it.next(), None);
}

#[test]
fn slice_with_unique_items() {
    let input = &mut [0, 1, 2];
    let mut it = super::new(input);

    assert_eq!(it.next(), Some(&mut [0][..]));
    assert_eq!(it.next(), Some(&mut [1][..]));
    assert_eq!(it.next(), Some(&mut [2][..]));
    assert_eq!(it.next(), None);
}

#[test]
fn slice_with_one_item_type() {
    let input = &mut [0, 0, 0, 0];
    let mut it = super::new(input);

    assert_eq!(it.next(), Some(&mut [0, 0, 0, 0][..]));
    assert_eq!(it.next(), None);
}

#[test]
fn slice_with_various_groups() {
    let input = &mut [0, 0, 1, 2, 2, 2];
    let mut it = super::new(input);

    assert_eq!(it.next(), Some(&mut [0, 0][..]));
    assert_eq!(it.next(), Some(&mut [1][..]));
    assert_eq!(it.next(), Some(&mut [2, 2, 2][..]));
    assert_eq!(it.next(), None);
}

#[test]
fn version_with_closure_condition() {
    let input = &mut [0_i32, -1, -2, 2, 2, 2];
    let mut it = super::new_with_comparison(input, |&a, &b| b.signum() == a.signum());

    assert_eq!(it.next(), Some(&mut [0][..]));
    assert_eq!(it.next(), Some(&mut [-1, -2][..]));
    assert_eq!(it.next(), Some(&mut [2, 2, 2][..]));
    assert_eq!(it.next(), None);
}

#[derive(Debug, PartialEq)]
struct Pair(i32, i32);

#[test]
fn version_with_closure_key() {
    let input = &mut [Pair(0, 0), Pair(0, 1), Pair(0, 2), Pair(1, 0), Pair(1, 1)];
    let mut it = super::new_with_key_getter(input, |a| a.0);

    assert_eq!(
        it.next(),
        Some(&mut [Pair(0, 0), Pair(0, 1), Pair(0, 2)][..])
    );
    assert_eq!(it.next(), Some(&mut [Pair(1, 0), Pair(1, 1)][..]));
    assert_eq!(it.next(), None);
}
