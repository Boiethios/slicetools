#[cfg(test)]
mod tests;

//use ::StreamerMut;
use std::marker::PhantomData;

pub struct GroupByMut<'a, T: 'a, K: 'a, KGetter, Compar>
where
    KGetter: Fn(&'a T) -> K,
    Compar: Fn(K, K) -> bool,
{
    ptr: *mut T,
    end: *mut T,
    _marker: PhantomData<&'a mut T>,
    key_getter: KGetter,
    comparison: Compar,
}

impl<'a, T, K: 'a, KGetter, Compar> Iterator for GroupByMut<'a, T, K, KGetter, Compar>
where
    KGetter: Fn(&'a T) -> K,
    Compar: Fn(K, K) -> bool,
{
    type Item = &'a mut [T];

    fn next(&mut self) -> Option<Self::Item> {
        if self.ptr == self.end {
            None
        } else {
            unsafe {
                let len = 1 + (1..)
                    .take_while(|&i| {
                        self.ptr.offset(i) != self.end
                            && (self.comparison)(
                                (self.key_getter)(&*self.ptr.offset(i)),
                                (self.key_getter)(&*self.ptr),
                            )
                    })
                    .count();
                let begin = self.ptr;

                self.ptr = self.ptr.offset(len as isize);
                Some(::std::slice::from_raw_parts_mut(begin, len))
            }
        }
    }
}

/* Constructors */

pub type Simple<'a, T> = GroupByMut<'a, T, &'a T, fn(&'a T) -> &'a T, fn(&'a T, &'a T) -> bool>;

pub fn new<'a, T>(slice: &'a mut [T]) -> Simple<'a, T>
where
    T: PartialEq,
{
    let (ptr, end) = slice_as_ptrs(slice);

    GroupByMut {
        ptr,
        end,
        _marker: PhantomData::default(),
        key_getter: identity,
        comparison: PartialEq::eq,
    }
}

pub type WithComparison<'a, T, C> = GroupByMut<'a, T, &'a T, fn(&'a T) -> &'a T, C>;

pub fn new_with_comparison<'a, T, C>(slice: &'a mut [T], comparison: C) -> WithComparison<'a, T, C>
where
    C: Fn(&'a T, &'a T) -> bool,
{
    let (ptr, end) = slice_as_ptrs(slice);

    GroupByMut {
        ptr,
        end,
        _marker: PhantomData::default(),
        key_getter: identity,
        comparison,
    }
}

pub type WithKeyGetter<'a, T, K, G> = GroupByMut<'a, T, K, G, fn(K, K) -> bool>;

pub fn new_with_key_getter<'a, T, K: 'a, G>(
    slice: &'a mut [T],
    key_getter: G,
) -> WithKeyGetter<'a, T, K, G>
where
    K: PartialEq,
    G: Fn(&'a T) -> K,
{
    let (ptr, end) = slice_as_ptrs(slice);

    GroupByMut {
        ptr,
        end,
        _marker: PhantomData::default(),
        key_getter,
        comparison: |a, b| a == b,
    }
}

/* Utils */

fn identity<T>(item: T) -> T {
    item
}

fn slice_as_ptrs<T>(slice: &mut [T]) -> (*mut T, *mut T) {
    let ptr = slice.as_mut_ptr();
    let end = unsafe { ptr.offset(slice.len() as isize) };

    (ptr, end)
}
