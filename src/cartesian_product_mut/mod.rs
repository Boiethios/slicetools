#[cfg(test)]
mod tests;

use BoundedIterator;

pub struct CartesianProductMut<'a, T: 'a, U: 'a> {
    slice: (&'a mut [T], &'a mut [U]),
    index: (usize, usize),
}

impl<'a, 'item, T: 'item, U: 'item> BoundedIterator<'item> for CartesianProductMut<'a, T, U> {
    type Item = (&'item mut T, &'item mut U);

    fn next_item(&'item mut self) -> Option<Self::Item> {
        if self.index.0 >= self.slice.0.len() {
            return None;
        }
        if self.index.1 >= self.slice.1.len() {
            self.index.0 += 1;
            self.index.1 = 0;
        }
        match self.slice.0.get_mut(self.index.0) {
            Some(first) => {
                let ret = Some((first, self.slice.1.get_mut(self.index.1).unwrap()));
                self.index.1 += 1;
                ret
            }
            None => None,
        }
    }
}

impl<'a, T, U> CartesianProductMut<'a, T, U> {
    pub fn new(s1: &'a mut [T], s2: &'a mut [U]) -> CartesianProductMut<'a, T, U> {
        CartesianProductMut {
            slice: (s1, s2),
            index: (0, 0),
        }
    }
}
