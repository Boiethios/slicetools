use super::CartesianProductMut;
use BoundedIterator;

#[test]
fn slices_when_at_least_one_is_empty() {
    let s1: &mut [i32] = &mut [];
    let s2: &mut [i32] = &mut [];

    {
        let mut it = CartesianProductMut::new(s1, s2);
        assert_eq!(it.next_item(), None);
    }

    let s1 = &mut [0];
    {
        let mut it = CartesianProductMut::new(s1, s2);
        assert_eq!(it.next_item(), None);
    }

    {
        let mut it = CartesianProductMut::new(s2, s1);
        assert_eq!(it.next_item(), None);
    }
}

#[test]
fn slices_with_1_item_each() {
    let s1 = &mut [0];
    let s2 = &mut ['a'];
    let mut it = CartesianProductMut::new(s1, s2);

    assert_eq!(it.next_item(), Some((&mut 0, &mut 'a')));
    assert_eq!(it.next_item(), None);
}

#[test]
fn slices_when_one_have_only_1_item() {
    let s1 = &mut [0, 1, 2];
    let s2 = &mut ['a'];

    {
        let mut it = CartesianProductMut::new(s1, s2);
        assert_eq!(it.next_item(), Some((&mut 0, &mut 'a')));
        assert_eq!(it.next_item(), Some((&mut 1, &mut 'a')));
        assert_eq!(it.next_item(), Some((&mut 2, &mut 'a')));
        assert_eq!(it.next_item(), None);
    }

    {
        let mut it = CartesianProductMut::new(s2, s1);
        assert_eq!(it.next_item(), Some((&mut 'a', &mut 0)));
        assert_eq!(it.next_item(), Some((&mut 'a', &mut 1)));
        assert_eq!(it.next_item(), Some((&mut 'a', &mut 2)));
        assert_eq!(it.next_item(), None);
    }
}

#[test]
fn slices_with_multiple_items() {
    let s1 = &mut [0, 1, 2];
    let s2 = &mut ['a', 'b'];

    {
        let mut it = CartesianProductMut::new(s1, s2);
        assert_eq!(it.next_item(), Some((&mut 0, &mut 'a')));
        assert_eq!(it.next_item(), Some((&mut 0, &mut 'b')));
        assert_eq!(it.next_item(), Some((&mut 1, &mut 'a')));
        assert_eq!(it.next_item(), Some((&mut 1, &mut 'b')));
        assert_eq!(it.next_item(), Some((&mut 2, &mut 'a')));
        assert_eq!(it.next_item(), Some((&mut 2, &mut 'b')));
        assert_eq!(it.next_item(), None);
    }

    {
        let mut it = CartesianProductMut::new(s2, s1);
        assert_eq!(it.next_item(), Some((&mut 'a', &mut 0)));
        assert_eq!(it.next_item(), Some((&mut 'a', &mut 1)));
        assert_eq!(it.next_item(), Some((&mut 'a', &mut 2)));
        assert_eq!(it.next_item(), Some((&mut 'b', &mut 0)));
        assert_eq!(it.next_item(), Some((&mut 'b', &mut 1)));
        assert_eq!(it.next_item(), Some((&mut 'b', &mut 2)));
        assert_eq!(it.next_item(), None);
    }
}
