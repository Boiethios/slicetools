use super::IsolateAllMut;
use BoundedIterator;

#[test]
fn slice_with_0_item() {
    let input: &mut [i32] = &mut [];
    let mut it = IsolateAllMut::new(input);

    assert!(it.next_item().is_none());
}

#[test]
fn slice_with_1_item() {
    let input = &mut [0];
    let mut it = IsolateAllMut::new(input);

    assert_eq!(it.next_item(), Some((&mut [][..], &mut 0, &mut [][..])));
    assert_eq!(it.next_item(), None);
}

#[test]
fn slice_with_multiple_items() {
    let input = &mut [0, 1, 2, 3];
    let mut it = IsolateAllMut::new(input);

    assert_eq!(
        it.next_item(),
        Some((&mut [][..], &mut 0, &mut [1, 2, 3][..]))
    );
    assert_eq!(
        it.next_item(),
        Some((&mut [0][..], &mut 1, &mut [2, 3][..]))
    );
    assert_eq!(
        it.next_item(),
        Some((&mut [0, 1][..], &mut 2, &mut [3][..]))
    );
    assert_eq!(
        it.next_item(),
        Some((&mut [0, 1, 2][..], &mut 3, &mut [][..]))
    );
    assert_eq!(it.next_item(), None);
}
