#[cfg(test)]
mod tests;

use BoundedIterator;

pub struct IsolateAllMut<'a, T: 'a> {
    slice: &'a mut [T],
    index: usize,
}

impl<'a, 'item, T: 'item> BoundedIterator<'item> for IsolateAllMut<'a, T> {
    type Item = (&'item mut [T], &'item mut T, &'item mut [T]);

    fn next_item(&'item mut self) -> Option<Self::Item> {
        let (begin, tmp) = self.slice.split_at_mut(self.index);

        match tmp.split_first_mut() {
            None => None,
            Some((item, end)) => {
                self.index += 1;
                Some((begin, item, end))
            }
        }
    }
}

impl<'a, T> IsolateAllMut<'a, T> {
    pub fn new(s: &'a mut [T]) -> IsolateAllMut<'a, T> {
        IsolateAllMut { slice: s, index: 0 }
    }
}
