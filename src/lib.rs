//! This crate aims to provide various tools for slices.
//!
//! This crate use its own streaming iterator
//! due to the lack of generic associated type in the language:
//! you therefore cannot use those iterators with the `for` control flow.
//! Use the `while let` control flow, or the macro helper, as you can see below.
//!
//! # Example
//!
//! ```
//! use slicetools::*;
//!
//! let mut v = vec![1, 2, 3, 4];
//! {
//!     let mut it = v.pairs_mut();
//!     
//!     while let Some((a, b)) = it.next_item() {
//!         if *b > *a {
//!             *a += 1;
//!         }
//!     }
//! }
//! assert_eq!(v, &[4, 4, 4, 4]);
//! ```
//!
//! Or, with the helper macro (do not forget to `#[macro_use]`):
//!
//! ```
//! use slicetools::*;
//!
//! let mut v = vec![1, 2, 3, 4];
//!
//! for_each!( (a, b) in v.pairs_mut() => {
//!     if *b > *a {
//!         *a += 1;
//!     }
//! });
//! assert_eq!(v, &[4, 4, 4, 4]);
//! ```

mod bounded_iterator;
pub use bounded_iterator::BoundedIterator;

mod cartesian_product_mut;
use cartesian_product_mut::CartesianProductMut;
//mod group_ref
mod group_mut;
mod isolate_all_mut;
use isolate_all_mut::IsolateAllMut;
mod pairs;
use pairs::PairsRef;
mod pairs_mut;
use pairs_mut::PairsMut;

/// Convenient helper to use easily the custom iterator `BoundedIterator`.
#[macro_export]
macro_rules! for_each {
    { $item:pat in $iter:expr => $b:block } => {{
        let mut it = $iter;

        while let Some($item) = it.next_item() {
            $b
        }
    }}
}

/// A trait to extend slices and `Vec`s with regular iterators.
pub trait SliceTools {
    /// Iterate on groups of equal items,
    /// yielding mutables references on slices.
    ///
    /// # Example
    ///
    /// ```
    /// use slicetools::*;
    ///
    /// let mut v = vec![100, 100, 100, 200, 200, 200, 200];
    /// {
    ///     let mut it = v.group_mut();
    ///     
    ///     while let Some(slice) = it.next() {
    ///         for (i, num) in slice.iter_mut().enumerate() {
    ///             *num += i;
    ///         }
    ///     }
    /// }
    /// assert_eq!(v, &[100, 101, 102, 200, 201, 202, 203]);
    /// ```
    fn group_mut<'a, T: 'a>(&'a mut self) -> group_mut::Simple<'a, T>
    where
        Self: AsMut<[T]>,
        T: PartialEq,
    {
        group_mut::new(self.as_mut())
    }

    /// Iterate on groups of equal items,
    /// yielding mutables references on slices.
    ///
    /// # Example
    ///
    /// ```
    /// use slicetools::*;
    ///
    /// let mut v = vec![(0, 10), (0, 10), (1, 10), (1, 10), (1, 10)];
    /// {
    ///     let mut it = v.group_by_key_mut(|a| a.0);
    ///     
    ///     while let Some(slice) = it.next() {
    ///         for (i, pair) in slice.iter_mut().enumerate() {
    ///             pair.1 += i as i32;
    ///         }
    ///     }
    /// }
    /// assert_eq!(v, &[(0, 10), (0, 11), (1, 10), (1, 11), (1, 12)]);
    /// ```
    fn group_by_key_mut<'a, T: 'a, K, G>(
        &'a mut self,
        key_getter: G,
    ) -> group_mut::WithKeyGetter<'a, T, K, G>
    where
        Self: AsMut<[T]>,
        K: PartialEq,
        G: Fn(&T) -> K,
    {
        group_mut::new_with_key_getter(self.as_mut(), key_getter)
    }

    /// Iterate on groups of equal items,
    /// yielding mutables references on slices.
    ///
    /// # Example
    ///
    /// ```
    /// use slicetools::*;
    ///
    /// let mut v = vec![0_i32, -1, -2, 2, 2, 2];
    /// {
    ///     let mut it = v.group_by_mut(|&a, &b| b.signum() == a.signum());
    ///     
    ///     while let Some(slice) = it.next() {
    ///         for (i, num) in slice.iter_mut().enumerate() {
    ///             *num += i as i32;
    ///         }
    ///     }
    /// }
    /// assert_eq!(v, &[0, -1, -1, 2, 3, 4]);
    /// ```
    fn group_by_mut<'a, T, C>(&'a mut self, comparison: C) -> group_mut::WithComparison<'a, T, C>
    where
        Self: AsMut<[T]>,
        C: Fn(&'a T, &'a T) -> bool,
    {
        group_mut::new_with_comparison(self.as_mut(), comparison)
    }

    /// Iterate on all the possible pairs of the slice,
    /// yielding references on items.
    ///
    /// # Example
    ///
    /// ```
    /// use slicetools::*;
    ///
    /// let v = vec![1, 2, 3];
    /// let mut it = v.pairs();
    ///
    /// assert_eq!(it.next(), Some((&1, &2)));
    /// assert_eq!(it.next(), Some((&1, &3)));
    /// assert_eq!(it.next(), Some((&2, &3)));
    /// assert_eq!(it.next(), None);
    /// ```
    fn pairs<'a, T: 'a>(&'a self) -> PairsRef<'a, T>
    where
        Self: AsRef<[T]>,
    {
        PairsRef::new(self.as_ref())
    }
}

/// A trait to extend slices and `Vec`s with custom iterators `BoundedIterator`.
pub trait SliceToolsBounded {
    /// Iterate on all the pairs (a, b) from slices A and B such as a ∈ A and b ∈ B,
    /// yielding mutables references on items.
    ///
    /// # Example
    ///
    /// ```
    /// use slicetools::*;
    ///
    /// let mut v1 = vec![100; 3];
    /// let mut v2 = vec![1, 2, 3, 4];
    /// {
    ///     let mut it = v1.cartesian_product_mut(&mut v2);
    ///     
    ///     while let Some((a, b)) = it.next_item() {
    ///         *a += *b;
    ///     }
    /// }
    /// assert_eq!(v1, &[110; 3]);
    /// ```
    fn cartesian_product_mut<'a, S, T: 'a, U: 'a>(
        &'a mut self,
        slice: &'a mut S,
    ) -> CartesianProductMut<'a, T, U>
    where
        Self: AsMut<[T]>,
        S: AsMut<[U]>,
    {
        CartesianProductMut::new(self.as_mut(), slice.as_mut())
    }

    /// Iterate on all the possible pairs of the slice,
    /// yielding mutables references on items.
    ///
    /// # Example
    ///
    /// ```
    /// use slicetools::*;
    ///
    /// let mut v = vec![1, 2, 3, 4];
    /// {
    ///     let mut it = v.pairs_mut();
    ///     
    ///     while let Some((a, b)) = it.next_item() {
    ///         if *b > *a {
    ///             *a += 1;
    ///         }
    ///     }
    /// }
    /// assert_eq!(v, &[4, 4, 4, 4]);
    /// ```
    fn pairs_mut<'a, T: 'a>(&'a mut self) -> PairsMut<'a, T>
    where
        Self: AsMut<[T]>,
    {
        PairsMut::new(self.as_mut())
    }

    /// Iterate on all the elements of a slice,
    /// yielding mutables references on items,
    /// and on the slices before and after the item.
    ///
    /// # Example
    ///
    /// ```
    /// use slicetools::*;
    ///
    /// let mut v = vec![1, 3, 2, 4, 4, 3];
    /// let mut collided = vec![];
    /// {
    ///     let mut it = v.isolate_all_mut();
    ///     
    ///     while let Some((before, item, after)) = it.next_item() {
    ///         if before.contains(item) || after.contains(item) {
    ///             collided.push(*item);
    ///             *item = 0;
    ///         }
    ///     }
    /// }
    /// assert_eq!(v, &[1, 0, 2, 0, 4, 3]);
    /// assert_eq!(collided, &[3, 4]);
    /// ```
    fn isolate_all_mut<'a, T: 'a>(&'a mut self) -> IsolateAllMut<'a, T>
    where
        Self: AsMut<[T]>,
    {
        IsolateAllMut::new(self.as_mut())
    }
}

impl<T> SliceTools for T {}
impl<T> SliceToolsBounded for T {}
