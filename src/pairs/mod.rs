#[cfg(test)]
mod tests;

pub struct PairsRef<'a, T: 'a> {
    slice: &'a [T],
    index: (usize, usize),
}

impl<'a, T> Iterator for PairsRef<'a, T> {
    type Item = (&'a T, &'a T);

    fn next(&mut self) -> Option<Self::Item> {
        let index = self.index.0;

        match self.slice.len() - index {
            0 | 1 => None,
            _ => {
                if self.index.1 >= self.slice.len() {
                    self.index = (self.index.0 + 1, self.index.0 + 2);
                }
                if self.index.1 < self.slice.len() {
                    debug_assert!(self.index.0 < self.index.1);
                    let res = (
                        self.slice.get(self.index.0).unwrap(),
                        self.slice.get(self.index.1).unwrap(),
                    ); //TODO pattern matching
                    self.index.1 += 1;
                    Some(res)
                } else {
                    None
                }
            }
        }
    }
}

impl<'a, T> PairsRef<'a, T> {
    pub fn new(slice: &'a [T]) -> PairsRef<'a, T> {
        PairsRef {
            slice,
            index: (0, 1),
        }
    }
}
