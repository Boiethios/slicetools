use super::PairsRef;

#[test]
fn empty_slice() {
    let input: &[i32] = &[];
    let mut it = PairsRef::new(input);

    assert!(it.next().is_none());
}

#[test]
fn slice_with_1_item() {
    let input = &[0];
    let mut it = PairsRef::new(input);

    assert_eq!(it.next(), None);
}

#[test]
fn slice_with_2_items() {
    let input = &[0, 1];
    let mut it = PairsRef::new(input);

    assert_eq!(it.next(), Some((&0, &1)));
    assert_eq!(it.next(), None);
}

#[test]
fn slice_with_more_items() {
    let input = &[0, 1, 2, 3];
    let mut it = PairsRef::new(input);

    assert_eq!(it.next(), Some((&0, &1)));
    assert_eq!(it.next(), Some((&0, &2)));
    assert_eq!(it.next(), Some((&0, &3)));
    assert_eq!(it.next(), Some((&1, &2)));
    assert_eq!(it.next(), Some((&1, &3)));
    assert_eq!(it.next(), Some((&2, &3)));
    assert_eq!(it.next(), None);
}
